<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Esercizio del 04/05/2020</title>
  </head>
  <body>
    <p> Sequenza di fibonacci dal primo al 100esimo numero </p>
    <?php
        function fibonacciHelper($a, $b, $n){
          echo("<br>" . $n . ": " . $a);
          if($n == 100) return;
          return fibonacciHelper($b, $a + $b, $n + 1);
        }

        fibonacciHelper(0, 1, 0);
    ?>
  </body>
</html>
